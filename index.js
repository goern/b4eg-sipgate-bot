const express = require('express');
const app = express();
const mqtt = require('mqtt');
const TelegramBot = require('node-telegram-bot-api');

const token = process.env.TELEGRAM_TOKEN;

const notifyIds = ['55924495', '433187040'];
const start = 'start';
const list = 'list';
const stop = 'stop';

// create all the clients we need...
let bot = new TelegramBot(token, {polling: true});
let mqttClient = mqtt.connect(process.env.CLOUDMQTT_URL);
let redisClient = require('redis').createClient(process.env.REDIS_URL);

bot.onText(/\/start/, (msg) => {
  const chatId = msg.chat.id;
  notifyId = msg.chat.id;

  console.log(notifyId);

  bot.sendMessage(chatId, 'This is what I can do for you...', {
    'reply_markup': {
      'keyboard': [['start sending notifications', 'stop sending notifications'], ['list of users receiving notifications']],
    },
  });
});

bot.onText(/\/list/, (msg) => {
  chatId = msg.chat.id;
  notifyId = msg.chat.id;

  bot.sendMessage(chatId, 'I am sending notifications to: ');
  notifyIds.forEach(function(notifyId) {
    bot.sendMessage(chatId, notifyId.toString());
  });
});

bot.on('message', (msg) => {
  chatId = msg.chat.id;
  notifyId = msg.chat.id;

  if (msg.text.indexOf(start) === 0) {
    bot.sendMessage(msg.chat.id, 'Ok ' + chatId + ' I will send you notifications...');
  }
  if (msg.text.indexOf(stop) === 0) {
    bot.sendMessage(msg.chat.id, 'stopped sending notifications to ' + chatId);
  }
  if (msg.text.toLowerCase().indexOf(list) === 0) {
    bot.sendMessage(chatId, 'I am sending notifications to: ');
    notifyIds.forEach(function(notifyId) {
      bot.sendMessage(chatId, notifyId.toString());
    });
  }
});

redisClient.on('error', function(err) {
  console.log('Error ' + err);
});

app.get('/_healthz', function(request, response) {
  response.send('OK');
});

mqttClient.on('connect', () => {
  mqttClient.subscribe('homeassistant/sipgate/call');
});

mqttClient.on('message', (topic, message) => {
  if (topic === 'homeassistant/sipgate/call') {
    // message looks like '2017-10-20T11:18:43.116Z, in, +492284062311, +492289659033'

    parts = message.split(',');
    redisClient.incr(parts[2]);

    for (let thisNotifyId of notifyIds) {
      bot.sendMessage(thisNotifyId, message.toString());
    }
  }
});

app.set('port', (process.env.PORT || 8080));

app.listen(app.get('port'), function() {
  console.log('Node app is running on port', app.get('port'));
});
